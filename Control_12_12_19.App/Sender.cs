﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Twilio;
using Twilio.Rest.Api.V2010.Account;

namespace Control_12_12_19.App
{
    public static class Sender
    {
        const string accountSid = "AC36e9eea5cdb83fc53e3d062329e9cf8a";
        const string authToken = "your_auth_token";
        const string fromNumber = "your_number";

        public static bool SendMessage(string toNumber, string message)
        {
            try
            {
                TwilioClient.Init(accountSid, authToken);

                var messageResource = MessageResource.Create(
                body: message,
                from: new Twilio.Types.PhoneNumber(fromNumber),
                to: new Twilio.Types.PhoneNumber(toNumber)
                );
                return true;
            }
            catch (Exception exception)
            {
                //Честно я не виноват
                return false;
            }
        }
    }
}
