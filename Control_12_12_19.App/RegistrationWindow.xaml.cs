﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;

namespace Control_12_12_19.App
{
    /// <summary>
    /// Interaction logic for RegistrationWindow.xaml
    /// </summary>
    public partial class RegistrationWindow : Window
    {
        private List<Account> existingAccounts;
        public Account NewAccount { get; set; }

        public RegistrationWindow(List<Account> accounts)
        {
            InitializeComponent();
            existingAccounts = accounts;
        }

        private void RegistrateButtonClick(object sender, RoutedEventArgs e)
        {
            string login = loginBox.Text;
            string password = passwordBox.Password;
            string number = numberBox.Text;
            
            if((string.IsNullOrWhiteSpace(login)) || 
               (string.IsNullOrWhiteSpace(password)) ||
               (string.IsNullOrWhiteSpace(number)))
            {
                MessageBox.Show("Заполните все поля!");
                return;
            }
            if (!IsPhoneNumber(number))
            {
                MessageBox.Show("Неверный формат номера!");
                return;
            }

            Account newAccount = new Account()
            {
                Login = login,
                Password = password,
                Number = number
            };
            if (!SameAccountExists(newAccount))
            {
                NewAccount = newAccount;
                MessageBox.Show("Спасибо за регистрацию!");
                this.DialogResult = true;
            }
            MessageBox.Show("Такой логин/телефон уже занят!");
        }
        private bool SameAccountExists(Account account)
        {
            var match = existingAccounts.Where(acc => (acc.Login == account.Login) ||
                                                      (acc.Number == account.Number))
                                                      .FirstOrDefault();
            if (match != null)
                return true;

            return false;
        }
        private bool IsPhoneNumber(string number)
        {
            //+77474443242
            return Regex.Match(number, @"^\+[0-9]{11}$").Success;
        }
    }
}
