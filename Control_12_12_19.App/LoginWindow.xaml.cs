﻿using System.Collections.Generic;
using System.Windows;

namespace Control_12_12_19.App
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        DbManager dbManager;
        List<Account> accounts;
        RegistrationWindow registrationWindow;
        public LoginWindow()
        {
            InitializeComponent();
            dbManager = new DbManager();
            accounts = dbManager.GetAllAccounts();
        }

        private void LoginButtonClick(object sender, RoutedEventArgs e)
        {
            string password = passwordBox.Password;
            string login = loginBox.Text;

            Account logined = Validate(login, password);
            if(logined != null)
            {
                BankWindow bankWindow = new BankWindow(logined);
                bankWindow.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show("Неверный логин или пароль!");
            }
        }

        private Account Validate(string login, string password)
        {
            foreach (var account in accounts)
            {
                if (account.Login == login)
                {
                    if (account.Password == password)
                        return account;
                }
            }
            return null;
        }

        private void RegistrationButtonClick(object sender, RoutedEventArgs e)
        {
            registrationWindow = new RegistrationWindow(accounts);
            if (registrationWindow.ShowDialog() == true)
            {
                dbManager.AddAccount(registrationWindow.NewAccount);
                accounts = dbManager.GetAllAccounts();
            }
        }
    }
}
