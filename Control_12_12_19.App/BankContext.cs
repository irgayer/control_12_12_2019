﻿using System.Data.Entity;

namespace Control_12_12_19.App
{
    public class BankContext : DbContext
    {
        public BankContext() : base("DbConnection") 
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<BankContext>());
        }

        public DbSet<Account> Accounts { get; set; }
    }
}
