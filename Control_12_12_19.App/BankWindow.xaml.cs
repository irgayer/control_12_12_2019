﻿using System;
using System.Windows;

namespace Control_12_12_19.App
{
    /// <summary>
    /// Interaction logic for BankWindow.xaml
    /// </summary>
    public partial class BankWindow : Window
    {
        Account loginedAccount;
        DbManager dbManager;
        private Func<string, string, bool> action;
        public BankWindow(Account loginedAccount)
        {
            InitializeComponent();
            dbManager = new DbManager();
            this.loginedAccount = loginedAccount;
            loginBlock.Text = loginedAccount.Login;
            moneyBlock.Text = loginedAccount.Money.ToString();
        }

        private void ConfirmButtonClick(object sender, RoutedEventArgs e)
        {
            if(int.TryParse(sumBox.Text, out int sum))
            {
                if (sum <= 0)
                {
                    MessageBox.Show("Ошибка!");
                    return;
                }
                if(sub.IsChecked == true)
                {
                    if(loginedAccount.Money >= sum)
                    {
                        loginedAccount.Money -= sum;
                        dbManager.UpdateAccount(loginedAccount);
                        
                        string message = $"С вашего счета сняли {sum} денег\nОстаток {loginedAccount.Money}";
                        action = new Func<string, string, bool>(Sender.SendMessage);
                        action.BeginInvoke(loginedAccount.Number, message, ProcessResult, null);
                    }
                    else
                    {
                        MessageBox.Show("Недостаточно средств!");
                    }
                }
                else if(add.IsChecked == true)
                {
                    loginedAccount.Money += sum;
                    dbManager.UpdateAccount(loginedAccount);

                    string message = $"На ваш счет добавили {sum} денег\nОстаток {loginedAccount.Money}";
                    action = new Func<string, string, bool>(Sender.SendMessage);
                    action.BeginInvoke(loginedAccount.Number, message, ProcessResult, null);
                }
                else
                {
                    MessageBox.Show("Выберите тип операции!");
                }
                moneyBlock.Text = loginedAccount.Money.ToString();
            }
            else
            {
                MessageBox.Show("Неверный формат суммы!");
            }
        }
        private void ProcessResult(IAsyncResult result)
        {
            bool success = action.EndInvoke(result);
            if(success == true)
            {
                MessageBox.Show("Операция прошла успешно!");
            }
            else
            {
                MessageBox.Show("Операция не прошла успешно!");
            }
        }
    }
}
