﻿namespace Control_12_12_19.App
{
    public class Account
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Number { get; set; }
        public int Money { get; set; } = 0;
    }
}
