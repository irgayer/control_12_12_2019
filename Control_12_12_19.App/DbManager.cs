﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Control_12_12_19.App
{
    public class DbManager
    {
        public List<Account> GetAllAccounts()
        {
            var result = new List<Account>();
            using (var context = new BankContext())
            {
                result = context.Accounts.ToList();
            }

            return result;
        }
        public void AddAccount(Account newAccount)
        {
            using(var context = new BankContext())
            {
                context.Accounts.Add(newAccount);
                context.SaveChanges();
            }
        }
        public void UpdateAccount(Account loginedAccount)
        {
            using (var context = new BankContext())
            {
                var accounts = context.Accounts;
                var account = accounts.Where(acc => acc.Id == loginedAccount.Id).FirstOrDefault();
                account = loginedAccount;

                context.SaveChanges();
            }
        }
    }
}
